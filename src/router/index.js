import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import David from "../views/david.vue";
import Todo from "../views/todo.vue";

import ResumeHome from "../views/resume/Home.vue";

Vue.use(VueRouter);

const routes = [
  // RESUME
  {
    path: "/resume",
    name: "resume",
    component: ResumeHome,
  },

  //

  {
    path: "/todo",
    name: "todo",
    component: Todo,
  },
  {
    path: "/david",
    name: "David",
    component: David,
  },

  {
    path: "/",
    name: "Home",
    component: Home,
  },

  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
